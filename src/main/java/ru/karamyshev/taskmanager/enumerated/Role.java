package ru.karamyshev.taskmanager.enumerated;

import org.jetbrains.annotations.Nullable;

public enum Role {

    USER("Пользователь"),
    ADMIN("Администратор");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public String getDisplayName() {
        return displayName;
    }
}
