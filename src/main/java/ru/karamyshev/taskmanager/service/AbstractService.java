package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IRepository;
import ru.karamyshev.taskmanager.api.service.IService;
import ru.karamyshev.taskmanager.model.AbstractEntitty;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntitty> implements IService<E> {

    @NotNull
    private final IRepository<E> iRepository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.iRepository = repository;
    }

    @Nullable
    @Override
    public List<E> getList() {
        return iRepository.getList();
    }

    @Override
    public void load(@Nullable List<E> eList) {
        if (eList == null) return;
        iRepository.load(eList);
    }

}
