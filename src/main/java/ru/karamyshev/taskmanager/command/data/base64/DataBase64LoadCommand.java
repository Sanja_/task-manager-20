package ru.karamyshev.taskmanager.command.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.dto.Domain;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String arg() {
        return "-dtbnld";
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    @SneakyThrows
    public void execute(){
        System.out.println("[DATA BASE64 LOAD]");

        final String dataBase64 = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        final byte[] data = Base64.getDecoder().decode(dataBase64);

        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        System.out.println("[OK]");

        serviceLocator.getAuthService().logout();
        System.out.println("[YOU ARE LOGGED OUT]");

    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
