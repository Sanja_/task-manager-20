package ru.karamyshev.taskmanager.command.data.xml.faster;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;

public class DataFasterXmlClearCommand extends AbstractDataCommand implements Serializable {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-fs-xml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove xml(faster) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE XML(FASTER) FILE]");
        final File file = new File(FILE_FAST_XML);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
