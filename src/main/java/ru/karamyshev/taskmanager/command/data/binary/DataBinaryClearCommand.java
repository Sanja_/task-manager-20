package ru.karamyshev.taskmanager.command.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;

public class DataBinaryClearCommand extends AbstractDataCommand implements Serializable {

    @NotNull
    @Override
    public String arg() {
        return "data-bin-clear";
    }

    @NotNull
    @Override
    public String name() {
        return "-dtbnclr";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove binary file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE BINARY FILE]");
        final File file = new File(FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
