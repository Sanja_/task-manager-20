package ru.karamyshev.taskmanager.command.data.xml.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.dto.Domain;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJaxXmlLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-jax-xml-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML(JAX-B) LOAD]");

        final File file = new File(FILE_JAX_XML);

        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller un = jaxbContext.createUnmarshaller();
        Domain domain = (Domain) un.unmarshal(file);
        setDomain(domain);
        System.out.println("[OK]");

        serviceLocator.getAuthService().logout();
        System.out.println("[YOU ARE LOGGED OUT]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
