package ru.karamyshev.taskmanager.command.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;

public class DataBase64ClearCommand extends AbstractDataCommand implements Serializable {

    @NotNull
    @Override
    public String arg() {
        return "-dtbsclr";
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove base64 file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE BASE64 FILE]");
        final File file = new File(FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
