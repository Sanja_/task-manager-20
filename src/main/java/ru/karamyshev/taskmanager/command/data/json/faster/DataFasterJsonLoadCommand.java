package ru.karamyshev.taskmanager.command.data.json.faster;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.dto.Domain;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataFasterJsonLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-fs-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON(FASTER) LOAD]");

        final String data = new String(Files.readAllBytes(Paths.get(FILE_FAST_JSON)));

        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = (Domain) objectMapper.readValue(data, Domain.class);

        setDomain(domain);
        System.out.println("[OK]");

        serviceLocator.getAuthService().logout();
        System.out.println("[YOU ARE LOGGED OUT]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
