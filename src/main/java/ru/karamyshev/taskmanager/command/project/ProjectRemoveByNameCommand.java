package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.model.Project;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.List;

public class ProjectRemoveByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtrmvnm";
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        final IAuthService authService = serviceLocator.getAuthService();
        final IProjectService projectService = serviceLocator.getProjectService();
        final String userId = authService.getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT NAME FOR DELETION:");
        final String name = TerminalUtil.nextLine();
        final List<Project> project = projectService.removeOneByName(userId, name);
        if (project == null || project.isEmpty()) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
