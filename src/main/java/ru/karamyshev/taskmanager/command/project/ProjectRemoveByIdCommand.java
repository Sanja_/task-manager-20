package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.model.Project;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtrmvid";
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        final IAuthService authService = serviceLocator.getAuthService();
        final IProjectService projectService = serviceLocator.getProjectService();
        final String userId = authService.getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID FOR DELETION:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
