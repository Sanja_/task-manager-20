package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.model.Project;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.List;

public class ProjectShowByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtvwnm";
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by name.";
    }

    @Override
    public void execute() {
        final IAuthService authService = serviceLocator.getAuthService();
        final IProjectService projectService = serviceLocator.getProjectService();
        final String userId = authService.getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final List<Project> project = projectService.findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        for (Project proj : project) {
            showProjects(proj);
        }
        System.out.println("[OK]");
    }

    @Nullable
    private void showProjects(final Project project) {
        if (project == null) return;
        System.out.println("ID:" + project.getId());
        System.out.println("NAME:" + project.getName());
        System.out.println("DESCRIPTION:" + project.getDescription());
    }

}
