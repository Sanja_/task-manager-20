package ru.karamyshev.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "ex";
    }

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
