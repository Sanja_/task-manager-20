package ru.karamyshev.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-lgin";
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login in account.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        System.out.println("[OK]");
    }

}
