package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UserRenamePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-rnm-psswrd";
    }

    @NotNull
    @Override
    public String name() {
        return "rename-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Rename password account.";
    }

    @Override
    public void execute() {
        IAuthService authService = serviceLocator.getAuthService();
        String userId = authService.getUserId();
        System.out.println("CHANGE ACCOUNT PASSWORD");
        System.out.println("[ENTER OLD PASSWORD]");
        String oldPassword = TerminalUtil.nextLine();
        System.out.println("[ENTER NEW PASSWORD]");
        String newPassword = TerminalUtil.nextLine();
        String currentLogin = authService.getCurrentLogin();
        authService.renamePassword(userId, currentLogin, oldPassword, newPassword);
        System.out.println("[OK]");
    }

}
