package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UserRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-rmvusr";
    }

    @NotNull
    @Override
    public String name() {
        return "remove-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove users";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        final String currentLogin = serviceLocator.getAuthService().getCurrentLogin();
        serviceLocator.getUserService().removeUserByLogin(currentLogin, login);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
