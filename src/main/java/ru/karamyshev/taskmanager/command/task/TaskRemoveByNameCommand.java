package ru.karamyshev.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.model.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.List;

public class TaskRemoveByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskrmvnm";
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {
        IAuthService authService = serviceLocator.getAuthService();
        ITaskService taskService = serviceLocator.getTaskService();
        final String userId = authService.getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK NAME FOR DELETION:");
        final String name = TerminalUtil.nextLine();
        final List<Task> task = taskService.removeOneByName(userId, name);
        if (task == null || task.isEmpty()) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
