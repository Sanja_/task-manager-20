package ru.karamyshev.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.model.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskupid";
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        IAuthService authService = serviceLocator.getAuthService();
        ITaskService taskService = serviceLocator.getTaskService();
        final String userId = authService.getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = taskService.updateTaskById(userId, id, name, description);
        if (taskUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
