package ru.karamyshev.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.model.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskrmvind";
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        IAuthService authService = serviceLocator.getAuthService();
        ITaskService taskService = serviceLocator.getTaskService();
        final String userId = authService.getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK INDEX FOR DELETION:");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = taskService.removeOneByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
