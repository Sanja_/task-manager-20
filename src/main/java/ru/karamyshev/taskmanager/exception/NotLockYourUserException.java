package ru.karamyshev.taskmanager.exception;

public class NotLockYourUserException extends RuntimeException {

    public NotLockYourUserException() {
        super("Error! You cannot lock your user...");
    }
}
