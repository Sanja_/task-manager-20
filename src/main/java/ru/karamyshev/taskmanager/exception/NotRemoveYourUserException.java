package ru.karamyshev.taskmanager.exception;

public class NotRemoveYourUserException extends RuntimeException {

    public NotRemoveYourUserException() {
        super("Error! You cannot remove your user...");
    }
}
