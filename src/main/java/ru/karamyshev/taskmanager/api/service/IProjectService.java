package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.model.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    void create(@Nullable String userId, @Nullable String name);

    @Nullable
    void create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    void add(@Nullable String userId, @Nullable Project project);

    @Nullable
    void remove(@Nullable String userId, @Nullable Project project);

    @Nullable
    List<Project> findAll(@Nullable String userId);

    @Nullable
    void clear(@Nullable String userId);

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    List<Project> findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project updateProjectById(@Nullable String userId,
                              @Nullable String id,
                              @Nullable String name,
                              @Nullable String description
    );

    @Nullable
    Project removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    List<Project> removeOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project updateProjectByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
