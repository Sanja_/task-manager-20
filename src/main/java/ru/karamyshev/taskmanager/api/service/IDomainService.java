package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.Domain;

public interface IDomainService {

    @Nullable
    void load(@Nullable Domain domain);

    @Nullable
    void export(@Nullable Domain domain);

}
