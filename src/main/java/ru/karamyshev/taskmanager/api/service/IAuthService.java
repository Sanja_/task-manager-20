package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.model.User;

public interface IAuthService {

    @Nullable
    String getUserId();

    @Nullable
    void checkRoles(@Nullable Role[] roles);

    @Nullable
    String getCurrentLogin();

    @Nullable
    boolean isAuth();

    @Nullable
    void login(@Nullable String login, @Nullable String password);

    @NotNull
    void logout();

    @Nullable
    void registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @Nullable
    void renameLogin(
            @Nullable String userId,
            @Nullable String currentLogin,
            @Nullable String newLogin
    );

    @Nullable
    User showProfile(@Nullable String userId, @Nullable String login);

    @Nullable
    void renamePassword(
            @Nullable String userId,
            @Nullable String currentLogin,
            @Nullable String oldPassword,
            @Nullable String newPassword
    );
}
