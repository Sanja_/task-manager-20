package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.model.AbstractEntitty;

import java.util.List;

public interface IRepository<E extends AbstractEntitty> {

    @Nullable
    List<E> findAll();

    @Nullable
    void clear();

    @Nullable
    List<E> getList();

    @NotNull
    void add(@NotNull List<E> eList);

    @Nullable
    void load(@NotNull List<E> eList);

}
