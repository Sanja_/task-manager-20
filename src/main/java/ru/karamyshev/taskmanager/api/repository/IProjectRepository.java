package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.model.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    @Nullable
    List<Project> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    List<Project> findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    List<Project> removeOneByName(@NotNull String userId, @NotNull String name);

}
