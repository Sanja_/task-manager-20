package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    void add(@NotNull String userId, @NotNull Task task);

    @Nullable
    void remove(@NotNull String userId, @NotNull Task task);

    @Nullable
    List<Task> findAll(@NotNull String userId);

    @Nullable
    void clear(@NotNull String userId);

    @Nullable
    Task findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    List<Task> findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    List<Task> removeOneByName(@NotNull String userId, @NotNull String name);

}
