package ru.karamyshev.taskmanager.dto;

import lombok.Getter;
import lombok.Setter;
import ru.karamyshev.taskmanager.model.Project;
import ru.karamyshev.taskmanager.model.Task;
import ru.karamyshev.taskmanager.model.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
public class  Domain implements Serializable {

    private List<Project> projects = new ArrayList<>();

    private List<Task> tasks = new ArrayList<>();

    private List<User> users = new ArrayList<>();

}
