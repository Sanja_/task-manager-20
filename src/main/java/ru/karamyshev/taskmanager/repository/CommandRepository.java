package ru.karamyshev.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.karamyshev.taskmanager.api.repository.ICommandRepository;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SuppressWarnings({"deprecation", "unchecked"})
public class CommandRepository implements ICommandRepository {

    final private List<AbstractCommand> commands = new ArrayList<>();

    {
        initCommand();
    }

    @SneakyThrows
    private void initCommand() {
        @NotNull final Reflections reflections = new Reflections("ru.karamyshev.taskmanager.command.");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.karamyshev.taskmanager.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            commands.add(clazz.newInstance());
        }
    }

    private final List<String> COMMANDS_LIST = getCommands(commands);

    private final List<String> ARGS = getArgs(commands);

    @Nullable
    public List<String> getCommands(@Nullable final List<AbstractCommand> values) {
        if (values == null || values.size() == 0) return null;
        final List<String> commandsList = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            final String command = values.get(i).name();
            if (command == null || command.isEmpty()) continue;
            commandsList.add(command);
        }
        return commandsList;
    }

    @Nullable
    public List<String> getArgs(@Nullable final List<AbstractCommand> values) {
        if (values == null || values.size() == 0) return null;
        final ArrayList<String> argsList = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            final String arg = values.get(i).arg();
            if (arg == null || arg.isEmpty()) continue;
            argsList.add(arg);
        }
        return argsList;
    }

    @NotNull
    public List<AbstractCommand> getTerminalCommands() {
        return commands;
    }

    @Nullable
    public List<String> getCommands() {
        return COMMANDS_LIST;
    }

    @Nullable
    public List<String> getArgs() {
        return ARGS;
    }
}
