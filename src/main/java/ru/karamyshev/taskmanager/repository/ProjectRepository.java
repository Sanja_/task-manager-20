package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        entities.add(project);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId())) return;
        entities.remove(project);
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : entities) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        final List<Project> projects = findAll(userId);
        entities.removeAll(projects);
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        for (final Project project : entities) {
            if (userId.equals(project.getUserId()) &&
                    Long.parseLong(id) == project.getId()) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeOneById(@NotNull final String userId, @NotNull final String id) {
        final Project task = findOneById(userId, id);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull String userId, @NotNull final Integer index) {
        final int arraySize = entities.size();
        int iteration = 0;
        for (final Project project : entities) {
            if (userId.equals(project.getUserId())) {
                if (index <= arraySize && iteration == index) return entities.get(index);
            }
            iteration++;
        }
        return null;
    }

    @Nullable
    @Override
    public List<Project> findOneByName(@NotNull final String userId, @NotNull final String name) {
        final List<Project> projectsSample = new ArrayList<>();
        for (final Project project : entities) {
            if (userId.equals(project.getUserId())) {
                if (name.equals(project.getName())) projectsSample.add(project);
            }
        }
        return projectsSample;
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public List<Project> removeOneByName(@NotNull final String userId, @NotNull final String name) {
        final List<Project> project = findOneByName(userId, name);
        if (project == null) return null;
        for (Project proj : project) {
            remove(userId, proj);
        }
        return project;
    }

}
