package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.model.AbstractEntitty;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntitty> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @Nullable
    public List<E> findAll() {
        return entities;
    }

    public void clear() {
        entities.clear();
    }

    public void add(final @NotNull List<E> es) {
        for (final E e : es) {
            if (e == null) return;
            entities.add(e);
        }
    }

    @Nullable
    public List<E> getList() {
        return entities;
    }

    public void load(@NotNull final List<E> es) {
        clear();
        add(es);
    }

}
