package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(final @NotNull String userId, final @NotNull Task task) {
        task.setUserId(userId);
        entities.add(task);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        entities.remove(task);
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : entities) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        final List<Task> task = findAll(userId);
        entities.removeAll(task);
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        for (final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (Long.parseLong(id) == task.getId()) return task;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final String userId, @NotNull final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final int arraySize = entities.size();
        int iteration = 0;
        for (final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (index <= arraySize && iteration == index) return entities.get(index);
            }
            iteration++;
        }
        return null;
    }

    @Nullable
    @Override
    public List<Task> findOneByName(@NotNull final String userId, @NotNull final String name) {
        final List<Task> projectsSample = new ArrayList<>();
        for (final Task task : entities) {
            if (userId.equals(task.getUserId())) {
                if (name.equals(task.getName())) projectsSample.add(task);
            }
        }
        return projectsSample;
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public List<Task> removeOneByName(@NotNull final String userId, @NotNull final String name) {
        final List<Task> tasks = findOneByName(userId, name);
        if (tasks == null) return null;
        for (Task task : tasks) {
            remove(userId, task);
        }
        return tasks;
    }

}
