package ru.karamyshev.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class AbstractEntitty implements Serializable {

    private static final long serialVersionUID = 1001L;

    private long id = UUID.randomUUID().getMostSignificantBits();

}
