# Информация о проекте.
## Приложение "Task Manager"
Приложение осуществляет запись задач и вывод информации по ним.

### Команды (Аргументы):
- help (-h) - доступные команды.
- about (-a) - информация о разработчике.
- version (-v) - версия приложения.
- info (-i) - информация о системе.
- exit - закрытие приложения.
- argument (-arg) - все аргументы для запуска через консоль.
- commands (-cmd) - все команды приложения.
- task-create - создание новой задачи.
- task-clear - удаление всех задач.
- task-list - вывод всех записанных задач.
- project-create - создание нового проекта.
- project-clear - удаление всех проектов.
- project-list - вывод всех записаннах проектов.
- task-update-by-index - обновление данных задачи по индексу.
- task-update-by-id - обновление данных задачи по id. 
- task-view-by-id - вывод данных задачи по id.
- task-view-by-index - вывод данных задачи по индексу.
- task-view-by-name - вывод данных задачи по имени.
- task-remove-by-id - удаление задачи по id.
- task-remove-by-index - удаление задачи по индексу.
- task-remove-by-name - удаление задачи по имени.
- project-update-by-index - обновление данных проекта по индексу.
- project-update-by-id - обновление данных проекта по id.
- project-view-by-id - вывод данных проекта по id.
- project-view-by-index - вывод данных проекта по индексу.
- project-view-by-name - вывод данных проекта по индексу.
- project-remove-by-id - удаление проекта по id.
- project-remove-by-index - удаление проекта по индексу.
- project-remove-by-name - удаление проекта по имени.
- login - вход в учётную запись.
- logout - выход из учётной записи.
- registry - регистрация новой учётной записи.
- show-profile - вывод информации об учётной записи.
- rename-password - изменение пароля учётной записи.
- rename-login - изменение логина учётной записи.

# Стек
- Java 8.
- IntelliJ IDEA.
- Maven 3.

# Аппаратное обеспечение.
Процессор: 
- Intel Core 2 Quad и выше.
- Amd Athlon 64 и выше. 

ОЗУ: 2гб.    

Графический память: 512 Мб.

Переферийные устройства: клавиатура, мышь.          
       
# Программное обеспечение.
- JDK 1.8.
- Windows 7.
- Maven 3.

# Сборка jar файла
``` 
mvn clean package 
```
# Запуск приложения.
 ```
 java -jar target/taskmanager-1.0.0.jar -h -v -a -i -arg -cmd
 ```

![](https://drive.google.com/uc?export=view&id=1m7QCkll66tcSVsT99UfMBNiw8xunk3ua)

### Ввод команд в консоль приложения

![](https://drive.google.com/uc?export=view&id=19Ek7piAn0bylORYvoHS3KFe-dOrs0mlL)

![](https://drive.google.com/uc?export=view&id=1kUmDgAlDw3pMAfjXO_3_8O9PyTWHKRzR)

![](https://drive.google.com/uc?export=view&id=1Uar8PauGaa7h9zWOxAr7_SiCODUbiNc6)

# Разработчики.
**Имя**: Александр Карамышев.

**Телефон:** 8(800)-555-35-35.

**Email**: sanja_19.96@mail.ru.
